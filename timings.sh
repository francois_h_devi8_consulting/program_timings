echo "Testing Python 2 - No Optimization"
time python hello.py >& /dev/null

echo "Testing Python 2"
time python -O hello.py >& /dev/null

echo "Testing Python 3 - No Optimization"
time python3 hello.py >& /dev/null

echo "Testing Python 3"
time python3 -O hello.py >& /dev/null

echo "Testing Node"
time node hello.js >& /dev/null

echo "Testing Assembly"
time ./helloasm >& /dev/null

echo "Testing C"
time ./helloc >& /dev/null

echo "Testing C++"
time ./hellocc >& /dev/null

echo "Testing Go Shell"
time go run hello.go >& /dev/null

echo "Testing Go Compiled"
time ./hellogo >& /dev/null

echo "Testing Java"
time java Main >& /dev/null

echo "Testing Swift Shell"
time swift -O hello.swift >& /dev/null

echo "Testing Swift Compiled"
time ./helloswift >& /dev/null

echo "Testing Dart"
time dart hello.dart >& /dev/null

echo "Testing Dart Compiled"
time ./hellodart >& /dev/null