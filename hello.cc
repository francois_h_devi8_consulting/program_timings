// g++ -O3 -o hellocc hello.cc
#include <iostream>

int main() {
  for (int i = 0; i < 10000000; i++) {
	 std::cout << "Hello, World " << i << std::endl;
  }
	return 0;
}
