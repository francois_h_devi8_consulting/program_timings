// gcc -O3 -o helloc hello.c
#include <stdio.h>

int main() {
  for (int i = 0; i < 10000000; i++) {
	 printf("Hello, World %d\n", i);
  }
	return 0;
}
